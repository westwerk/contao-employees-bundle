<?php
$GLOBALS['TL_LANG']['tl_ww_employees']['name'][0] = "Nachname";
$GLOBALS['TL_LANG']['tl_ww_employees']['firstname'][0] = "Vorname";
$GLOBALS['TL_LANG']['tl_ww_employees']['title'][0] = "Titel";
$GLOBALS['TL_LANG']['tl_ww_employees']['image'][0] = "Bild";
$GLOBALS['TL_LANG']['tl_ww_employees']['hover_image'][0] = "Bild für Hovereffekt";
$GLOBALS['TL_LANG']['tl_ww_employees']['phone'][0] = "Telefonnummer";
$GLOBALS['TL_LANG']['tl_ww_employees']['mobile'][0] = "Mobilnummer";
$GLOBALS['TL_LANG']['tl_ww_employees']['email'][0] = "E-Mail-Adresse";
$GLOBALS['TL_LANG']['tl_ww_employees']['published'][0] = "Veröffentlicht";
$GLOBALS['TL_LANG']['tl_ww_employees']['cart'][0] = "Info Box";