<?php

/**
 * Back end modules
 */
$GLOBALS['BE_MOD']['content']['employees'] = array(
    'tables' => array('tl_ww_employees','tl_ww_employees_projects','tl_content'),
    'icon' => 'system/themes/default/images/forward.gif'
);

$GLOBALS['TL_CTE']['westwerk']['projectemployeereader'] = 'Westwerk\\EmployeesBundle\\Classes\\ProjectEmployeeReader';
$GLOBALS['TL_CTE']['westwerk']['employeereader'] = 'Westwerk\\EmployeesBundle\\Classes\\EmployeeReader';
$GLOBALS['TL_CTE']['westwerk']['employeereaderdetail'] = 'Westwerk\\EmployeesBundle\\Classes\\EmployeeReaderDetail';

$GLOBALS['TL_LANG']['projects']['projects_headline'] = "Meine Projekte";