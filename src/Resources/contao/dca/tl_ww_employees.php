<?php
$GLOBALS['TL_DCA']['tl_ww_employees'] = [
    'config' => [
        'dataContainer' => 'Table',
        'ctable' => 'tl_ww_employees_projects',
        'enableVersioning' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ]
        ]
    ],
    'list' => [
        'sorting' => [
            'mode' => 5,
            'filter' => true,
            'headerFields' => ['name'],
            'flag' => 1,
            'panelLayout' => 'filter;sort,search,limit',
            'paste_button_callback' => array('tl_ww_employees', 'pasteElement')
        ],
        'label' => [
            'fields' => ['name'],
            'format' => '%s',
            'showColumns' => true,
        ],
        'global_operations' => [
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['edit'],
                'href' => 'table=tl_ww_employees_projects',
                'icon' => 'edit.gif'
            ],
            'editheader' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['editheader'],
                'href' => 'act=edit',
                'icon' => 'header.gif',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'paste' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['paste'],
                'href' => 'act=paste',
                'icon' => 'cut.gif',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
            ]
        ]
    ],
    'palettes' => [
        '__selector__' => [],
        'default' => '
            name,
            firstname,
            title,
        image,
        hover_image,
        phone,
        mobile,
        email,
        projects,
        published,
        cart'
    ],
    'subpalettes' => [
        '' => ''
    ],
    'fields' => [
        'id' => [
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ],
        'pid' => [
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ],
        'sorting' => array
        (
            'label' => &$GLOBALS['TL_LANG']['MSC']['sorting'],
            'sorting' => true,
            'flag' => 2,
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'name' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['name'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 255,
            ],
            'sql' => "varchar(255) NOT NULL default ''"
        ],
        'firstname' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['firstname'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 255
            ],
            'sql' => "varchar(255) NOT NULL default ''"
        ],
        'title' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['title'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'textarea',
            'sql' => "text NOT NULL default ''"
        ],
        'image' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['image'],
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'filesOnly' => true,
                'fieldType' => 'radio',
            ],
            'sql' => "binary(16) NULL"
        ],
        'hover_image' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['hover_image'],
            'exclude' => true,
            'inputType' => 'fileTree',
            'eval' => [
                'filesOnly' => true,
                'fieldType' => 'radio',
            ],
            'sql' => "binary(16) NULL"
        ],
        'phone' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['phone'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 255
            ],
            'sql' => "varchar(255) NOT NULL default ''"
        ],
        'mobile' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['mobile'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 255
            ],
            'sql' => "varchar(255) NOT NULL default ''"
        ],
        'email' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['email'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => [
                'maxlength' => 255
            ],
            'sql' => "varchar(255) NOT NULL default ''"
        ],
        'published' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['published'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'checkbox',
            'eval' => [
                'submitOnChange' => true,
                'doNotCopy' => true,
                'class' => 'w50'
            ],
            'sql' => "char(1) NOT NULL default '1'"
        ],
        'cart' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['cart'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'checkbox',
            'eval' => [
              'class' => 'w50'
            ],
            'sql' => "char(1) NOT NULL default '0'"
        ],

    ]
];
$GLOBALS['TL_DCA']['tl_ww_employees']['list']['operations']['toggle'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['toggle'],
    'icon' => 'visible.gif',
    'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
    'button_callback' => array('tl_ww_employees', 'toggleIcon')
);

class tl_ww_employees extends Backend
{

    public function pasteElement(DataContainer $dc, $row, $table)
    {
        $imagePasteAfter = Image::getHtml('pasteafter.gif', sprintf($GLOBALS['TL_LANG'][$table]['pasteafter'][1], $row['id']));
        if ($this->Input->get('id') == 0) {
            return '<a href="' . $this->addToUrl('act=create&mode=2&pid=' . $row['id']) . '" title="' . specialchars(sprintf($GLOBALS['TL_LANG'][$table]['pasteafter'][1], $row['id'])) . '" onclick="Backend.getScrollOffset()">' . $imagePasteAfter . '</a> ';
        }
        return '<a href="' . $this->addToUrl('act=cut&mode=1&pid=' . $row['id']) . '" title="' . specialchars(sprintf($GLOBALS['TL_LANG'][$table]['pasteafter'][1], $row['id'])) . '" onclick="Backend.getScrollOffset()">' . $imagePasteAfter . '</a> ';

    }

    /**
     * Ändert das Aussehen des Toggle-Buttons.
     * @param $row
     * @param $href
     * @param $label
     * @param $title
     * @param $icon
     * @param $attributes
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        $this->import('BackendUser', 'User');

        if (strlen($this->Input->get('tid'))) {
            $this->toggleVisibility($this->Input->get('tid'), ($this->Input->get('state') == 0));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_ww_employees::published', 'alexf')) {
            return '';
        }

        $href .= '&amp;id=' . $this->Input->get('id') . '&amp;tid=' . $row['id'] . '&amp;state=' . $row[''];

        if (!$row['published']) {
            $icon = 'invisible.gif';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . specialchars($title) . '"' . $attributes . '>' . $this->generateImage($icon, $label) . '</a> ';
    }

    /**
     * Toggle the visibility of an element
     * @param integer
     * @param boolean
     */
    public function toggleVisibility($intId, $blnPublished)
    {
        // Check permissions to publish
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_ww_employees::published', 'alexf')) {
            $this->log('Not enough permissions to show/hide record ID "' . $intId . '"', 'tl_ww_employees toggleVisibility', TL_ERROR);
            $this->redirect('contao/main.php?act=error');
        }

        $this->createInitialVersion('tl_ww_employees', $intId);

        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_ww_employees']['fields']['published']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_ww_employees']['fields']['published']['save_callback'] as $callback) {
                $this->import($callback[0]);
                $blnPublished = $this->$callback[0]->$callback[1]($blnPublished, $this);
            }
        }

        // Update the database
        $this->Database->prepare("UPDATE tl_ww_employees SET tstamp=" . time() . ", published='" . ($blnPublished ? '' : '1') . "' WHERE id=?")
            ->execute($intId);
        $this->createNewVersion('tl_ww_employees', $intId);
    }


}