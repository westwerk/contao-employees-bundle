<?php
// dca/tl_content.php
/**
 * Table tl_content
 */
$strName = 'tl_content';
/* Palettes */
$GLOBALS['TL_DCA'][$strName]['palettes']['projectemployeereader'] = '{type_legend},type,employee_page;';
$GLOBALS['TL_DCA'][$strName]['palettes']['employeereader'] = '{type_legend},type,employee_page;';
$GLOBALS['TL_DCA'][$strName]['palettes']['employeereaderdetail'] = '{type_legend},type;';
$GLOBALS['TL_DCA'][$strName]['fields']['employee_page'] = array
(
    'label' => &$GLOBALS['TL_LANG']['ww']['page'],
    'exclude' => true,
    'search' => true,
    'inputType' => 'pageTree',
    'sql' => "blob NULL"
);