<?php
$GLOBALS['TL_DCA']['tl_ww_employees_projects'] = [
    'config' => [
        'dataContainer' => 'Table',
        'switchToEdit' => true,
        'enableVersioning' => true,
        'ptable' => 'tl_ww_employees',
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ]
        ]
    ],
    'list' => [
        'sorting' => [
            'mode' => 1,
            'fields' => ['page_id'],
            'headerFields' => ['page_id'],
            'flag' => 1,
            'panelLayout' => 'debug;filter;sort,search,limit',
        ],
        'label' => [
            'fields' => ['tl_page.title'],
            'format' => '%s',
            'showColumns' => true,
            'label_callback' => array('EmployeesProject', 'getPageTitle')
        ],
        'global_operations' => [
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ],
            'editheader' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['editheader'],
                'href' => 'table=tl_ww_employees',
                'icon' => 'header.gif',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"',
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif'
            ]
        ]
    ],
    'palettes' => [
        '__selector__' => [],
        'default' => '
            page_id'
    ],
    'subpalettes' => [
        '' => ''
    ],
    'fields' => [
        'id' => [
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ],
        'page_id' => [
            'label' => &$GLOBALS['TL_LANG']['tl_ww_employees']['project'],
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'pageTree',
            'eval' => [
                'mandatory' => true,
                'maxlength' => 255,
            ],
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ],
        'pid' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'foreignKey' => 'tl_ww_employees.name',
            'relation' => array('type' => 'belongsTo', 'load' => 'lazy')

        ],

    ]
];

class EmployeesProject extends \Backend
{
    public function getPageTitle($row, $label)
    {
        $pageId = $row['page_id'];
        $this->import('Database');
        $result = $this->Database->prepare("SELECT * FROM tl_page WHERE id = ?")->execute($pageId);
        $newLabel = $result->pageTitle ? $result->pageTitle : $result->title;
        return $newLabel;
    }
}