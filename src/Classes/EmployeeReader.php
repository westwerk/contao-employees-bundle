<?php

namespace Westwerk\EmployeesBundle\Classes;

class EmployeeReader extends \ContentElement
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_employee_reader';


    protected function compile()
    {

        if (TL_MODE == 'BE') {
            $this->strTemplate = 'be_wildcard';
            $this->Template = new \BackendTemplate($this->strTemplate);
            $this->Template->wildcard = "### Employees ###";
        } else {

            $this->Template->employees = $this->getAllEmployees();
            $assetsDir = 'web/bundles/westwerkemployees';

            $GLOBALS['TL_JAVASCRIPT'][] = $assetsDir . '/js/app.min.js|static';
            $GLOBALS['TL_JAVASCRIPT'][] = $assetsDir . '/vendor/jquery.fancybox.min.js|static';
            $GLOBALS['TL_CSS'][] = $assetsDir . '/css/app.min.css||static';
            $GLOBALS['TL_CSS'][] = $assetsDir . '/vendor/jquery.fancybox.min.css||static';

        }
    }

    private function getAllEmployees()
    {
        $arr = array();
        $this->import('Database');
        $result = $this->Database->prepare("SELECT * FROM tl_ww_employees WHERE tl_ww_employees.published = 1  GROUP BY tl_ww_employees.id ORDER BY tl_ww_employees.sorting")->execute();
        while ($result->next()) {
            array_push($arr, [
                'id' => $result->id,
                'name' => $result->name,
                'firstname' => $result->firstname,
                'title' => $result->title,
                'phone' => $result->phone,
                'mobile' => $result->mobile,
                'email' => $result->email,
                'image' => $result->image,
                'hover_image' => $result->hover_image,
                'cart' => $result->cart,
            ]);
        }
        return $arr;
    }

}