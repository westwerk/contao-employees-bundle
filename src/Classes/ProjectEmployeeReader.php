<?php

namespace Westwerk\EmployeesBundle\Classes;

class ProjectEmployeeReader extends \ContentElement
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_project_employee_reader';


    protected function compile()
    {

        if (TL_MODE == 'BE') {
            $this->strTemplate = 'be_wildcard';
            $this->Template = new \BackendTemplate($this->strTemplate);
            $this->Template->wildcard = "### Projects's employees ###";
        } else {
            $this->import('Input');
            if ($this->Input->get('employeeId')) {
                $this->strTemplate('ce_employee_reader_detail');
            } else {

                $pageId = $GLOBALS['objPage']->id;
                $this->Template->employees = $this->getEmployees($pageId);
                $assetsDir = 'web/bundles/westwerkemployees';

                $GLOBALS['TL_JAVASCRIPT'][] = $assetsDir . '/js/app.min.js|static';
                $GLOBALS['TL_JAVASCRIPT'][] = $assetsDir . '/vendor/jquery.fancybox.min.js|static';
                $GLOBALS['TL_CSS'][] = $assetsDir . '/css/app.min.css||static';
                $GLOBALS['TL_CSS'][] = $assetsDir . '/vendor/jquery.fancybox.min.css||static';
            }
        }
    }

    private function getEmployees($pageId)
    {
        $arr = array();
        $this->import('Database');
        $result = $this->Database->prepare("SELECT tl_ww_employees_projects.pid as pid, tl_ww_employees.name as name, tl_ww_employees.firstname as firstname, tl_ww_employees.title as title, tl_ww_employees.phone as phone, tl_ww_employees.mobile as mobile, tl_ww_employees.email as email, tl_ww_employees.image as image, tl_ww_employees.hover_image as hover_image FROM tl_ww_employees LEFT JOIN tl_ww_employees_projects ON tl_ww_employees.id = tl_ww_employees_projects.pid WHERE tl_ww_employees.published = 1 AND tl_ww_employees_projects.page_id = ? GROUP BY tl_ww_employees.id,tl_ww_employees.pid,tl_ww_employees.name,tl_ww_employees.firstname,tl_ww_employees.title,tl_ww_employees.phone,tl_ww_employees.mobile,tl_ww_employees.email,tl_ww_employees.image,tl_ww_employees.hover_image ORDER BY name")->execute($pageId);
        while ($result->next()) {
            array_push($arr, [
                'id' => $result->pid,
                'name' => $result->name,
                'firstname' => $result->firstname,
                'title' => $result->title,
                'phone' => $result->phone,
                'mobile' => $result->mobile,
                'email' => $result->email,
                'image' => $result->image,
                'hover_image' => $result->hover_image
            ]);
        }

        return $arr;
    }
}
