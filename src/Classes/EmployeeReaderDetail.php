<?php

namespace Westwerk\EmployeesBundle\Classes;
class EmployeeReaderDetail extends \ContentElement
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'ce_employee_reader_detail';


    protected function compile()
    {

        if (TL_MODE == 'BE') {
            $this->strTemplate = 'be_wildcard';
            $this->Template = new \BackendTemplate($this->strTemplate);
            $this->Template->wildcard = "### Employee Reader Detail ###";
        } else {
            $this->import('Input');
            if ($eid = $this->Input->get('employee_id')) {

                $this->Template->employee = $this->getEmployeeById($eid);

            }
        }
    }


    public function getEmployeeById($id)
    {
        $arr = array();
        $prodArr = array();
        $this->import('Database');
        $result = $this->Database->prepare("SELECT tl_ww_employees.id,tl_ww_employees.name,tl_ww_employees.firstname,tl_ww_employees.title,tl_ww_employees.phone,tl_ww_employees.mobile,tl_ww_employees.email,tl_ww_employees.image,tl_ww_employees.hover_image FROM tl_ww_employees WHERE tl_ww_employees.published = 1 AND tl_ww_employees.id = ? GROUP BY tl_ww_employees.id")->execute($id);
        while ($result->next()) {
            $prodResult =  $this->Database->prepare("SELECT tl_news.headline,tl_news.singleSRC,tl_news.jumpTo,tl_ww_employees_projects.page_id FROM tl_ww_employees_projects LEFT JOIN tl_page ON tl_ww_employees_projects.page_id = tl_page.id LEFT JOIN tl_news ON tl_page.id = tl_news.jumpTo WHERE tl_news.published = 1 AND tl_ww_employees_projects.pid = ? GROUP BY tl_news.headline,tl_news.singleSRC,tl_news.jumpTo,tl_ww_employees_projects.page_id")->execute($result->id);
            while ($prodResult->next()) {
                array_push($prodArr, [
                    "title" => $prodResult->headline,
                    "image" => $prodResult->singleSRC,
                    "jumpTo" => $prodResult->jumpTo
                ]);
            }

            array_push($arr, [
                'id' => $result->id,
                'name' => $result->name,
                'firstname' => $result->firstname,
                'title' => $result->title,
                'phone' => $result->phone,
                'mobile' => $result->mobile,
                'email' => $result->email,
                'image' => $result->image,
                'hover_image' => $result->hover_image,
                'projects' => $prodArr
            ]);
        }
        return $arr;
    }
}