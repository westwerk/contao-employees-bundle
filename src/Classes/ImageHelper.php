<?php
/**
 * Class ImageHelper
 */
namespace Westwerk\EmployeesBundle\Classes;
class ImageHelper
{

    /**
     * Gibt den HTML-String für die Ausgabe des angepassten Bilds zurück.
     * @param        $strUuid
     * @param        $strSize
     * @param string $strAlt
     * @param string $strTitle
     * @return string
     */
    public static function getImage($strUuid, $strSize, $strAlt = '', $strTitle = '')
    {
        $strPath = self::getPath($strUuid, $strSize);
        return \Contao\Image::getHtml($strPath, $strAlt, $strTitle);
    }

    /**
     * Bekommt eine Uuid und den Size-String aus der Tabellen, bearbeitet das Bild und gibt den Pfad zurück.
     * @param $strUuid
     * @param $strSize
     * @return string
     */
    public static function getPath($strUuid, $arrSize)
    {
        $objFileModel = \Contao\FilesModel::findById($strUuid);

        if ($objFileModel) {

            if (count($arrSize) == 3) {
                // Bildgöße anpassen
                $strPath    = $objFileModel->path;
                $intWidth   = $arrSize[0];
                $intHight   = $arrSize[1];
                $strMode    = $arrSize[2];
                $strSrc     = self::getResizedPath($strPath, $intWidth, $intHight, $strMode);
                return $strSrc;
            }

            // Bildgröße nicht anpassen
            return $objFileModel->path;
        }

        // Kein Bild gefunden
        return '';
    }

    /**
     * Gibt den Pfad zum angepassten Bild zurück.
     * @param        $strPath
     * @param int    $intWidth
     * @param int    $intHeight
     * @param string $strMode
     * @return string
     */
    protected static function getResizedPath($strPath, $intWidth = 0, $intHeight = 0, $strMode = 'center_center')
    {
        $objFile       = new \File($strPath);
        $objImg        = new \Contao\Image($objFile);

        if ($intWidth) {
            $objImg->setTargetWidth($intWidth);
        }

        if ($intHeight) {
            $objImg->setTargetHeight($intHeight);
        }

        if ($strMode) {
            $objImg->setResizeMode($strMode);
        }

        $objImg->executeResize();

        return $objImg->getResizedPath();
    }
}
